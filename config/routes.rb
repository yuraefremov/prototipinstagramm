Rails.application.routes.draw do 
  devise_for :users
  root to: "users#index"
  
  resources :users, only: [:show, :index]
  get 'comments/create'
  resources :posts do
    resources :comments
  end

end
