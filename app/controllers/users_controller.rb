class UsersController < ApplicationController
   
  def index
     @users = User.all
  end



  def show
    @user = User.find(params[:id])
    @posts = @user.posts.order(created_at: :desc).paginate(:page => params[:page], :per_page => 10)
  end


  private
    def set_user
      @user = User.find(params[:id])
    end
   
    def user_params
      params.require(:user).permit(:name,:avatar)
    end
end

 
