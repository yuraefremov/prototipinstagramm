class Post < ApplicationRecord
  belongs_to :user
  has_many_attached :images
  has_many :comments
  validates :content, presence: true, length: { maximum: 250 }
  acts_as_votable
end
