class Comment < ApplicationRecord
  belongs_to :post
  validates :author, presence: true, length: { maximum: 50 }
  validates :body, presence: true, length: { maximum: 250 }
end
