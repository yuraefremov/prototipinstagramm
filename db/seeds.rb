# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
10.times do 
  User.create(name: 'Yura', email: 'yura@inbox.ru', password:'1qaz@WSX', password_confirmation: '1qaz@WSX', )
  Post.create(content: Faker::Lorem.paragraph, user_id: '1')
  User.create(name: 'Petr', email: 'petr@inbox.ru', password:'1qaz@WSX', password_confirmation: '1qaz@WSX', )
  Post.create(content: Faker::Lorem.paragraph, user_id: '2')
  User.create(name: 'Vasya', email: 'vasya@bb.ru', password:'1qaz@WSX', password_confirmation: '1qaz@WSX', )
  Post.create(content: Faker::Lorem.paragraph, user_id: '3')
  User.create(name: 'Igor', email: 'igor@example.com', password:'1qaz@WSX', password_confirmation: '1qaz@WSX', )
  Post.create(content: Faker::Lorem.paragraph, user_id: '4')
  User.create(name: 'Max', email: 'maks@inbox.ru', password:'1qaz@WSX', password_confirmation: '1qaz@WSX', )
  Post.create(content: Faker::Lorem.paragraph, user_id: '5')
  User.create(name: 'Mirbek', email: 'mirbek@inbox.ru', password:'1qaz@WSX', password_confirmation: '1qaz@WSX', )
  Post.create(content: Faker::Lorem.paragraph, user_id: '6')
  User.create(name: 'Fufel', email: 'fufel_kuy@example.com', password:'1qaz@WSX', password_confirmation: '1qaz@WSX')
 Post.create(content: Faker::Lorem.paragraph, user_id: '7')
end